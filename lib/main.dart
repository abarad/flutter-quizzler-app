import 'package:flutter/material.dart';
import 'package:quizzler/quiz.dart';
import 'package:quizzler/quiz_page.dart';
import 'package:quizzler/loading.dart';
import 'package:quizzler/error.dart';

void main() => runApp(Quizzler());

class Quizzler extends StatelessWidget {
  final Quiz quiz = Quiz();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: SafeArea(
            child: Text(
              'QUIZZLER',
              style: TextStyle(
                letterSpacing: 2.0,
                fontSize: 28.0,
              ),
            ),
          ),
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.grey.shade900,
        ),
        backgroundColor: Colors.grey.shade900,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: FutureBuilder(
              future: quiz.fetchQuestions(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return QuizPage(quiz: snapshot.data);
                } else if (snapshot.hasError) {
                  return error;
                } else {
                  return loader;
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
