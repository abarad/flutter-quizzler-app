import 'package:loading/loading.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_spin_fade_loader_indicator.dart';

final loader = Center(
  child: Loading(
    indicator: BallSpinFadeLoaderIndicator(),
    size: 100.0,
  ),
);
