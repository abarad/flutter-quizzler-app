import 'package:flutter/material.dart';
import 'package:quizzler/quiz.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class QuizPage extends StatefulWidget {
  final Quiz quiz;
  QuizPage({Key key, this.quiz}) : super(key: key);

  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Icon> scoreKeeper = [];

  void showFinishAlert(int correctAnswers) {
    Alert(
      style: AlertStyle(
        isCloseButton: false,
        alertBorder: Border.all(color: Colors.transparent),
        backgroundColor: Colors.grey.shade800,
        titleStyle: TextStyle(
          color: Colors.white,
        ),
      ),
      context: context,
      title: 'You have finished the quiz.',
      content: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          widget.quiz.getScoreString(),
          style: TextStyle(color: Colors.white, fontSize: 18.0),
        ),
      ),
      buttons: [
        DialogButton(
          color: Colors.red,
          child: Text(
            'TRY AGAIN',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
          onPressed: () {
            setState(() {
              widget.quiz.reset();
              scoreKeeper = [];
            });
            Navigator.pop(context);
          },
        ),
        DialogButton(
          color: Colors.green,
          child: Text(
            'NEW GAME',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
          onPressed: () async {
            await widget.quiz.fetchQuestions();
            setState(() {
              scoreKeeper = [];
              widget.quiz.reset();
              Navigator.pop(context);
            });
          },
        ),
      ],
    ).show();
  }

  void checkAnswer(bool userPickedAnswer) {
    setState(() {
      if (widget.quiz.isAnswerCorrect(userPickedAnswer)) {
        scoreKeeper.add(
          Icon(
            Icons.check,
            color: Colors.green,
          ),
        );
      } else {
        scoreKeeper.add(
          Icon(
            Icons.close,
            color: Colors.red,
          ),
        );
      }
    });
    if (widget.quiz.isFinished()) {
      showFinishAlert(widget.quiz.getCorrectAnswers());
    }
    widget.quiz.nextQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                widget.quiz.getQuestionText(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              textColor: Colors.white,
              color: Colors.green,
              child: Text(
                'True',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28.0,
                ),
              ),
              onPressed: () {
                checkAnswer(true);
              },
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              color: Colors.red,
              child: Text(
                'False',
                style: TextStyle(
                  fontSize: 28.0,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                checkAnswer(false);
              },
            ),
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: scoreKeeper,
          ),
        )
      ],
    );
  }
}
