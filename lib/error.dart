import 'package:flutter/material.dart';

final error = Center(
  child: Text(
    'ERROR',
    style: TextStyle(
      color: Colors.red,
      fontSize: 24.0,
    ),
  ),
);
