import 'package:quizzler/question.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:html_unescape/html_unescape.dart';

class Quiz {
  int _questionNumber = 0;
  int _correctAnswers = 0;
  String _apiURL = 'https://opentdb.com/api.php?amount=10&type=boolean';

  List<Question> _questionBank = [];

  void nextQuestion() {
    if (_questionNumber < _questionBank.length - 1) {
      _questionNumber++;
    }
  }

  Future<Quiz> fetchQuestions() async {
    if (_questionBank.length != 0) {
      _questionBank = [];
    }
    final response = await http.get(_apiURL);
    final List<dynamic> data =
        await convert.jsonDecode(response.body)['results'];
    for (dynamic item in data) {
      _questionBank.add(
        Question(
            text: HtmlUnescape().convert(item['question']),
            answer: item['correct_answer'].toLowerCase() == 'true'),
      );
    }
    return this;
  }

  String getQuestionText() {
    return _questionBank[_questionNumber].text;
  }

  bool getCorrectAnswer() {
    return _questionBank[_questionNumber].answer;
  }

  bool isFinished() {
    return _questionNumber == _questionBank.length - 1;
  }

  int getCorrectAnswers() {
    return _correctAnswers;
  }

  int getNumberOfQuestions() {
    return _questionBank.length;
  }

  bool isAnswerCorrect(bool userPickedAnswer) {
    if (getCorrectAnswer() == userPickedAnswer) {
      _correctAnswers++;
      return true;
    }
    return false;
  }

  String getScoreString() {
    return 'Score: ${_correctAnswers.toString()} / ${getNumberOfQuestions().toString()}';
  }

  void reset() {
    _questionNumber = 0;
    _correctAnswers = 0;
  }
}
